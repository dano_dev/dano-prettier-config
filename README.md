# @dano/prettier-config

다노 프론트엔드 프로젝트에서 사용할 수 있는 Prettier 설정 🛠

## 설치

```bash
$ npm install --save-dev @dano/prettier-config
```

## 설정 적용

`package.json` 파일에 `prettier` 항목을 `@dano/prettier-config`으로 설정합니다.

```json
{
  "name": "@dano/some-project",
  "prettier": "@dano/prettier-config"
}
```

## 설정 커스터마이징

`@dano/prettier-confing` 설정을 기본으로 두고 다른 설정 값을 변경해야 할 경우 `.prettierrc.js` 또는 `prettier.config.js` 파일을 프로젝트 루트에 생성한 뒤 다음과 같이 작성합니다:

```js
module.exports = {
  ...require('@dano/prettier-config'),
  printWidth: 120,
};
```

만약 '공통적으로 적용되면 좋겠다'하는 옵션을 변경하시는 경우 말씀 주시면 논의 후에 반영할 수 있도록 하겠습니다 :)

## 참고

- [코드 컨벤션](https://wiki.dano.me/pages/viewpage.action?pageId=58951727)
